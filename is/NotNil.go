package is

import (
	"reflect"

	"gitlab.com/ljpcore/golib/test"
)

// NotNilAssertion provides the assertion logic for NotNil.
type NotNilAssertion struct{}

var _ test.Assertion = &NotNilAssertion{}

// NotNil determines if a value is not equal to nil.  An interface with a type
// but no value is considered nil.
var NotNil = &NotNilAssertion{}

// Evaluate evaluates the provided value in the context of the assertion.
func (*NotNilAssertion) Evaluate(x interface{}) test.Result {
	xt := reflect.TypeOf(x)

	if x != nil {
		xv := reflect.ValueOf(x)
		if !isKindNilable(xv.Kind()) || !xv.IsNil() {
			return test.PassedResult
		}
	}

	return test.Result{
		Pass:    false,
		Message: "expected value to not be nil",
		Specifics: map[string]interface{}{
			"Type": xt,
			"x":    x,
		},
	}
}
