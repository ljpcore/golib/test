package is

import (
	"bytes"
	"io"
	"os"
	"testing"
)

func TestNil(t *testing.T) {
	testCases := []struct {
		given  interface{}
		expect bool
	}{
		{given: "Hello, World!", expect: false},
		{given: 5, expect: false},
		{given: false, expect: false},
		{given: true, expect: false},
		{given: nil, expect: true},
		{given: (*bytes.Buffer)(nil), expect: true},
		{given: map[string]string(nil), expect: true},
		{given: []byte(nil), expect: true},
		{given: (func() bool)(nil), expect: true},
		{given: chan int(nil), expect: true},
		{given: io.Writer((*os.File)(nil)), expect: true},
	}

	for i, testCase := range testCases {
		// Arrange and Act.
		r := Nil.Evaluate(testCase.given)

		// Assert.
		if r.Pass != testCase.expect {
			t.Fatalf("expected testCase %v Pass to be %v but was %v: %v", i, testCase.expect, r.Pass, r.Message)
		}
	}
}
