package is

import (
	"reflect"

	"gitlab.com/ljpcore/golib/test"
)

// NotDeepEqualToAssertion provides the assertion logic for NotDeepEqualTo.
type NotDeepEqualToAssertion struct {
	y interface{}
}

var _ test.Assertion = &NotDeepEqualToAssertion{}

// NotDeepEqualTo determines if one value is not deep-equal to another,
// according to the rules of the reflect package.
func NotDeepEqualTo(y interface{}) *NotDeepEqualToAssertion {
	return &NotDeepEqualToAssertion{y: y}
}

// Evaluate evaluates the provided value in the context of the assertion.
func (a *NotDeepEqualToAssertion) Evaluate(x interface{}) test.Result {
	if reflect.DeepEqual(x, a.y) {
		return test.Result{
			Pass:    false,
			Message: "expected not deep-equal values",
			Specifics: map[string]interface{}{
				"x     ": x,
				"x Type": reflect.TypeOf(x),
				"y     ": a.y,
				"y Type": reflect.TypeOf(a.y),
			},
		}
	}

	return test.PassedResult
}
