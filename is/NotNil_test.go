package is

import (
	"bytes"
	"testing"
)

func TestNotNil(t *testing.T) {
	testCases := []struct {
		given  interface{}
		expect bool
	}{
		{given: "Hello, World!", expect: true},
		{given: 5, expect: true},
		{given: false, expect: true},
		{given: true, expect: true},
		{given: nil, expect: false},
		{given: (*bytes.Buffer)(nil), expect: false},
		{given: map[string]string(nil), expect: false},
		{given: []byte(nil), expect: false},
		{given: (func() bool)(nil), expect: false},
		{given: chan int(nil), expect: false},
	}

	for i, testCase := range testCases {
		// Arrange and Act.
		r := NotNil.Evaluate(testCase.given)

		// Assert.
		if r.Pass != testCase.expect {
			t.Fatalf("expected testCase %v Pass to be %v but was %v: %v", i, testCase.expect, r.Pass, r.Message)
		}
	}
}
