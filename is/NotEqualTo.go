package is

import (
	"reflect"

	"gitlab.com/ljpcore/golib/test"
)

// NotEqualToAssertion provides the assertion logic for NotEqualTo.
type NotEqualToAssertion struct {
	y interface{}
}

var _ test.Assertion = &NotEqualToAssertion{}

// NotEqualTo determines if one value is not equal to another.  This assertion
// uses the standard != condition on two values of type interface{} when
// evaluating. You may prefer to use is.NotDeepEqualTo which evaluates using the
// deep-equal rules of the reflect package.
func NotEqualTo(y interface{}) *NotEqualToAssertion {
	return &NotEqualToAssertion{y: y}
}

// Evaluate evaluates the provided value in the context of the assertion.
func (a *NotEqualToAssertion) Evaluate(x interface{}) test.Result {
	if x == a.y {
		return test.Result{
			Pass:    false,
			Message: "expected not equal values",
			Specifics: map[string]interface{}{
				"x     ": x,
				"x Type": reflect.TypeOf(x),
				"y     ": a.y,
				"y Type": reflect.TypeOf(a.y),
			},
		}
	}

	return test.PassedResult
}
