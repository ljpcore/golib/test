package is

import (
	"reflect"

	"gitlab.com/ljpcore/golib/test"
)

// EqualToAssertion provides the assertion logic for EqualTo.
type EqualToAssertion struct {
	y interface{}
}

var _ test.Assertion = &EqualToAssertion{}

// EqualTo determines if one value is equal to another.  This assertion uses the
// standard == condition on two values of type interface{} when evaluating.
// You may prefer to use is.DeepEqualTo which evaluates using the deep-equal
// rules of the reflect package.
func EqualTo(y interface{}) *EqualToAssertion {
	return &EqualToAssertion{y: y}
}

// Evaluate evaluates the provided value in the context of the assertion.
func (a *EqualToAssertion) Evaluate(x interface{}) test.Result {
	if x != a.y {
		return test.Result{
			Pass:    false,
			Message: "expected equal values",
			Specifics: map[string]interface{}{
				"x     ": x,
				"x Type": reflect.TypeOf(x),
				"y     ": a.y,
				"y Type": reflect.TypeOf(a.y),
			},
		}
	}

	return test.PassedResult
}
