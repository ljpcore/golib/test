package is

import "testing"

func TestTrue(t *testing.T) {
	testCases := []struct {
		given  interface{}
		expect bool
	}{
		{given: "Hello, World!", expect: false},
		{given: 5, expect: false},
		{given: false, expect: false},
		{given: true, expect: true},
	}

	for i, testCase := range testCases {
		// Arrange and Act.
		r := True.Evaluate(testCase.given)

		// Assert.
		if r.Pass != testCase.expect {
			t.Fatalf("expected testCase %v Pass to be %v but was %v: %v", i, testCase.expect, r.Pass, r.Message)
		}
	}
}
