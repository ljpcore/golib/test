package is

import (
	"io"
	"os"
	"testing"
)

func TestEqualTo(t *testing.T) {
	testCases := []struct {
		x      interface{}
		y      interface{}
		expect bool
	}{
		{x: 4, y: 5, expect: false},
		{x: int16(5), y: 5, expect: false},
		{x: 5, y: 5, expect: true},
		{x: "Hello, World", y: "Hello, World!", expect: false},
		{x: "Hello, World!", y: "Hello, World!", expect: true},
		{x: nil, y: nil, expect: true},
		{x: [2]byte{}, y: [2]byte{}, expect: true},
		{x: io.Writer((*os.File)(nil)), y: nil, expect: false},
	}

	for i, testCase := range testCases {
		// Arrange and Act.
		r := EqualTo(testCase.y).Evaluate(testCase.x)

		// Assert.
		if r.Pass != testCase.expect {
			t.Fatalf("expected testCase %v Pass to be %v but was %v: %v", i, testCase.expect, r.Pass, r.Message)
		}
	}
}
