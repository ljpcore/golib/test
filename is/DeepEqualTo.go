package is

import (
	"reflect"

	"gitlab.com/ljpcore/golib/test"
)

// DeepEqualToAssertion provides the assertion logic for DeepEqualTo.
type DeepEqualToAssertion struct {
	y interface{}
}

var _ test.Assertion = &DeepEqualToAssertion{}

// DeepEqualTo determines if one value is deep-equal to another, according to
// the rules of the reflect package.
func DeepEqualTo(y interface{}) *DeepEqualToAssertion {
	return &DeepEqualToAssertion{y: y}
}

// Evaluate evaluates the provided value in the context of the assertion.
func (a *DeepEqualToAssertion) Evaluate(x interface{}) test.Result {
	if !reflect.DeepEqual(x, a.y) {
		return test.Result{
			Pass:    false,
			Message: "expected deep-equal values",
			Specifics: map[string]interface{}{
				"x     ": x,
				"x Type": reflect.TypeOf(x),
				"y     ": a.y,
				"y Type": reflect.TypeOf(a.y),
			},
		}
	}

	return test.PassedResult
}
