package is

import "testing"

func TestNotEqualTo(t *testing.T) {
	testCases := []struct {
		x      interface{}
		y      interface{}
		expect bool
	}{
		{x: 4, y: 5, expect: true},
		{x: int16(5), y: 5, expect: true},
		{x: 5, y: 5, expect: false},
		{x: "Hello, World", y: "Hello, World!", expect: true},
		{x: "Hello, World!", y: "Hello, World!", expect: false},
		{x: nil, y: nil, expect: false},
		{x: [2]byte{}, y: [2]byte{}, expect: false},
	}

	for i, testCase := range testCases {
		// Arrange and Act.
		r := NotEqualTo(testCase.y).Evaluate(testCase.x)

		// Assert.
		if r.Pass != testCase.expect {
			t.Fatalf("expected testCase %v Pass to be %v but was %v: %v", i, testCase.expect, r.Pass, r.Message)
		}
	}
}
