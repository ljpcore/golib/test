package has

import "testing"

func TestLength(t *testing.T) {
	c1 := make(chan int, 2)
	c2 := make(chan int, 2)
	c1 <- 0
	c2 <- 0
	c2 <- 0

	testCases := []struct {
		given  interface{}
		expect bool
	}{
		{given: "Hello, World!", expect: false},
		{given: "He", expect: true},
		{given: []byte{1, 2, 3}, expect: false},
		{given: []byte{1, 2}, expect: true},
		{given: [1]byte{}, expect: false},
		{given: [2]byte{}, expect: true},
		{given: map[int]int{0: 0}, expect: false},
		{given: map[int]int{0: 0, 1: 1}, expect: true},
		{given: c1, expect: false},
		{given: c2, expect: true},
		{given: 5, expect: false},
	}

	for i, testCase := range testCases {
		// Arrange and Act.
		r := Length(2).Evaluate(testCase.given)

		// Assert.
		if r.Pass != testCase.expect {
			t.Fatalf("expected testCase %v Pass to be %v but was %v: %v", i, testCase.expect, r.Pass, r.Message)
		}
	}
}
