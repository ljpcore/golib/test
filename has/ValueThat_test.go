package has

import (
	"testing"

	"gitlab.com/ljpcore/golib/test/is"
)

func TestValueThat(t *testing.T) {
	testCases := []struct {
		given  interface{}
		expect bool
	}{
		{given: "Hello, World!", expect: false},
		{given: map[string]int{"b": 0}, expect: false},
		{given: map[string]int{"a": 0, "b": 1}, expect: false},
		{given: map[string]int{"a": 0, "b": 1, "c": 2}, expect: true},
		{given: []int{0, 1}, expect: false},
		{given: []int{0, 1, 2}, expect: true},
		{given: [2]int{1, 3}, expect: false},
		{given: [2]int{2, 1}, expect: true},
		{given: []int{}, expect: false},
	}

	for i, testCase := range testCases {
		// Arrange and Act.
		r := ValueThat(is.EqualTo(2)).Evaluate(testCase.given)

		// Assert.
		if r.Pass != testCase.expect {
			t.Fatalf("expected testCase %v Pass to be %v but was %v: %v", i, testCase.expect, r.Pass, r.Message)
		}
	}
}
