package has

import (
	"reflect"
	"testing"
)

func TestType(t *testing.T) {
	typeStr := reflect.TypeOf("")
	typeInt := reflect.TypeOf(5)
	typeInt64 := reflect.TypeOf(int64(5))
	typeMap := reflect.TypeOf(map[string]string{})

	testCases := []struct {
		given  interface{}
		t      reflect.Type
		expect bool
	}{
		{given: "Hello, World!", t: typeStr, expect: true},
		{given: "Hello, World!", t: typeInt, expect: false},
		{given: "Hello, World!", t: typeMap, expect: false},
		{given: 5, t: typeInt, expect: true},
		{given: 5, t: typeInt64, expect: false},
		{given: int64(5), t: typeInt, expect: false},
		{given: int64(5), t: typeInt64, expect: true},
		{given: map[string]int{}, t: typeMap, expect: false},
		{given: map[string]string{}, t: typeMap, expect: true},
	}

	for i, testCase := range testCases {
		// Arrange and Act.
		r := Type(testCase.t).Evaluate(testCase.given)

		// Assert.
		if r.Pass != testCase.expect {
			t.Fatalf("expected testCase %v Pass to be %v but was %v: %v", i, testCase.expect, r.Pass, r.Message)
		}
	}
}
