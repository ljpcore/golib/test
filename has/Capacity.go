package has

import (
	"fmt"
	"reflect"

	"gitlab.com/ljpcore/golib/test"
)

// CapacityAssertion provides the assertion logic for Capacity.
type CapacityAssertion struct {
	y int
}

var _ test.Assertion = &CapacityAssertion{}

// Capacity determines if the provided array, channel, map, slice or string has
// a given capacity.
func Capacity(y int) *CapacityAssertion {
	return &CapacityAssertion{y: y}
}

// Evaluate evaluates the provided value in the context of the assertion.
func (a *CapacityAssertion) Evaluate(x interface{}) test.Result {
	allowedKinds := map[reflect.Kind]struct{}{
		reflect.Array: {},
		reflect.Chan:  {},
		reflect.Slice: {},
	}

	xv := reflect.ValueOf(x)
	xt := xv.Type()
	xk := xt.Kind()

	if _, ok := allowedKinds[xk]; !ok {
		return test.Result{
			Pass:    false,
			Message: "expected a type that has a capacity",
			Specifics: map[string]interface{}{
				"Type": xt,
			},
		}
	}

	xc := xv.Cap()
	if xc != a.y {
		return test.Result{
			Pass:    false,
			Message: fmt.Sprintf("expected a capacity of %v, but was %v", a.y, xc),
			Specifics: map[string]interface{}{
				"Actual Capacity  ": xc,
				"Expected Capacity": a.y,
				"Type             ": xt,
			},
		}
	}

	return test.PassedResult
}
