package has

import (
	"testing"

	"gitlab.com/ljpcore/golib/test/is"
)

func TestKeyThat(t *testing.T) {
	testCases := []struct {
		given  interface{}
		expect bool
	}{
		{given: "Hello, World!", expect: false},
		{given: map[string]int{"b": 0}, expect: false},
		{given: map[string]int{"a": 0}, expect: true},
		{given: map[string]int{}, expect: false},
	}

	for i, testCase := range testCases {
		// Arrange and Act.
		r := KeyThat(is.EqualTo("a")).Evaluate(testCase.given)

		// Assert.
		if r.Pass != testCase.expect {
			t.Fatalf("expected testCase %v Pass to be %v but was %v: %v", i, testCase.expect, r.Pass, r.Message)
		}
	}
}
