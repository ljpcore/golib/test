package has

import (
	"fmt"
	"reflect"

	"gitlab.com/ljpcore/golib/test"
)

// TypeAssertion provides the assertion logic for Type.
type TypeAssertion struct {
	y reflect.Type
}

var _ test.Assertion = &TypeAssertion{}

// Type determines if the provided value has the type provided.
func Type(y reflect.Type) *TypeAssertion {
	return &TypeAssertion{y: y}
}

// Evaluate evaluates the provided value in the context of the assertion.
func (a *TypeAssertion) Evaluate(x interface{}) test.Result {
	xt := reflect.TypeOf(x)
	if xt == a.y {
		return test.PassedResult
	}

	return test.Result{
		Pass:    false,
		Message: fmt.Sprintf("expected value to have type %v, but had type %v", a.y, xt),
		Specifics: map[string]interface{}{
			"Actual Type  ": xt,
			"Expected Type": a.y,
		},
	}
}
