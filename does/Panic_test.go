package does

import "testing"

func TestPanic(t *testing.T) {
	f1 := 5
	f2 := func(x int) {}
	f3 := func() bool { return false }
	f4 := func() { panic("ahh!") }
	f5 := func() {}

	testCases := []struct {
		given  interface{}
		expect bool
	}{
		{given: f1, expect: false},
		{given: f2, expect: false},
		{given: f3, expect: false},
		{given: f4, expect: true},
		{given: f5, expect: false},
	}

	for i, testCase := range testCases {
		// Arrange and Act.
		r := Panic.Evaluate(testCase.given)

		// Assert.
		if r.Pass != testCase.expect {
			t.Fatalf("expected testCase %v Pass to be %v but was %v: %v", i, testCase.expect, r.Pass, r.Message)
		}
	}
}
